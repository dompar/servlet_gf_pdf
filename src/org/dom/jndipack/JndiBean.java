package org.dom.jndipack;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.ejb.Singleton;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;


@Singleton
public class JndiBean {

    public String getJNDIProperties()
    {
        Properties props = new Properties();
        Gson gson;
        gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

        try
        {
            Context ctx = new InitialContext();
            props = (Properties) ctx.lookup("jndi/ConfigServer");
        }
        catch (NamingException e)
        {
            e.printStackTrace();
        }
        finally
        {
            Trans2json url_json = new Trans2json(props.getProperty("url_ws_professions", "Non trouvée"));

            String jsonRepresentation = gson.toJson(url_json);

            return jsonRepresentation;
        }

    }

}
