package org.dom.jndipack;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.io.UnsupportedEncodingException;

@Stateless
@Path("/url")
public class JndiResource {

    @EJB
    private DynamicListBean Liste;

    @EJB
    private JndiBean jndires;

    @Context
    private HttpServletResponse response;
    /**
     * Retrieves representation of an instance of JndiResource.JndiBean
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("text/json")
    //@Path("name={name}-editor={editor}")
    //public String getXml(@PathParam("name")  String name, @PathParam("editor")  String editor) {

    @Path("/query")
    public String getXml( @DefaultValue("peaudezob") @QueryParam("name") String name, @DefaultValue("rien")  @QueryParam("editor")  String editor) {
        //
        // Recuperation de la ligne complete (info)
        // public String getXml(@Context UriInfo info)


        // Pour l'appel ==> http://192.168.1.24:8080/GetJdniResource/resources/url/query

        //return "<html><body><h1>Hello </h1><a href=\"www.google.fr\">Adresse du Service web</a></body></html>";
        //return "<html><body><h1>Hello </h1><a href=\""+jndires.getJNDIProperties()+"\">Adresse du Service web</a></body></html>";

        /* ça c'est bien moi, trifouilleur de classe ...*/

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "x-http-method-override");

        response.setCharacterEncoding("UTF-8");

        return jndires.getJNDIProperties(); //+"_"+name+"_"+editor;

        //return "<html><body><h1>Hello "+nameStorage.getName()+"!</h1></body></html>";
    }


    @GET
    @Produces("text/json")
    @Path("/liste")
    public String getListe( @DefaultValue("neant") @QueryParam("idliste") String idliste) throws UnsupportedEncodingException {
        //
        // Recuperation de la ligne complete (info)
        // public String getXml(@Context UriInfo info)


        //http://dom-p5k-se-epu:8080/GetJdniResource/resources/url/liste?idliste=LSTPROFAFF

        //return "<html><body><h1>Hello </h1><a href=\""+jndires.getJNDIProperties()+"\">Adresse du Service web</a></body></html>";

        /* Pour le cross-domain : ça c'est bien moi, trifouilleur de classe ...*/
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, UPDATE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "x-http-method-override");

        response.setCharacterEncoding("UTF-8");

        return Liste.GetJSONList(idliste); //+"_"+name+"_"+editor;

        //return "<html><body><h1>Hello "+nameStorage.getName()+"!</h1></body></html>";
    }

    /**
     * PUT method for updating an instance of HelloWorldResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("text/plain")
    public void putXml(String content) {

        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "PUT, POST, GET, UPDATE, OPTIONS");
        response.addHeader("Access-Control-Allow-Headers", "x-http-method-override");

    }
}
