package org.dom.jndipack;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import javax.ejb.Singleton;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;


/** Singleton session bean used to store the name parameter for "/liste" resource
 *
 * @author Dominique
 */
@Singleton
public class DynamicListBean {


    public  Connection connection;
    private Statement statement;
    private InitialContext context;

    /**
     *
     * @param p_idlist
     * @return
     * @throws NamingException
     */
    public String GetJSONList(String p_idlist) throws UnsupportedEncodingException
    {
        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
        ArrayList<HashMap> professions = new ArrayList<HashMap>();

        //Hashtable professions = new Hashtable();
        HashMap<String, ArrayList<HashMap>> tabLignes=new  HashMap<String, ArrayList<HashMap>>();

        Gson gson;
        gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE).create();

        try
        {
            ResultSet rs;
            context = new InitialContext();

            DataSource dataSource = (DataSource) context.lookup("jdbc/emnh");
            connection = dataSource.getConnection();
            statement = connection.createStatement();

            //rs = statement.executeQuery("alter session set NLS_LANGUAGE='WE8CP850'");
            rs = statement.executeQuery("select a.id_val,a.valeur,b.nom_liste from val_listes a,listes b where a.id_liste='"+p_idlist.replaceAll("'", "\\''")+"' and a.id_liste = b.id_liste");

            int i;
            String tableau = "";

            //resultat
            while (rs.next())
            {

                HashMap<String,String> ligne = new HashMap<String,String>();
                ligne.put("id", rs.getString(1));
                ligne.put("value", rs.getString(2));
                tableau =  rs.getString(3);

                professions.add(ligne);
            }


            tabLignes.put(tableau,professions);

        }
        catch (Exception e  )
        {
            connection = null;
            //e.printStackTrace();
        }
        finally
        {
            String jsonRepresentation = java.net.URLDecoder.decode(gson.toJson(tabLignes), "UTF-8");

            return jsonRepresentation;
        }
    }
}
